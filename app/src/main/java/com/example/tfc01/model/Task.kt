package com.example.tfc01.model

import android.content.res.Resources
import com.example.tfc01.R

class Task constructor(
    private var description  : String,
    private var isUrgent     : Boolean,
    private var isCompleted  : Boolean
){
    var id: Long = 0

    //TODO - CRIAR CONSTRUTOR
    constructor(id: Long, description: String, isUrgent: Boolean, isCompleted: Boolean) : this(description, isUrgent, isCompleted) {
        this.id = id
        this.description = description
        this.isUrgent = isUrgent
        this.isCompleted = isCompleted
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun getDescription(): String {
        return this.description
    }

    fun getIsUrgent(): Boolean {
        return this.isUrgent
    }

    fun setIsCompleted(isCompleted: Boolean) {
        this.isCompleted = isCompleted
    }

    fun getIsCompleted(): Boolean {
        return this.isCompleted
    }

    override fun toString(): String {
        val isUrgentString: String = if(isUrgent){"(Urgent)"}else{"(Not Urgent)"}

        return "$isUrgentString $description"
    }
}