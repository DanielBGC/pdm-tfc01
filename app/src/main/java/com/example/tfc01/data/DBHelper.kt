package com.example.tfc01.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context): SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        private const val DB_NAME = "task_app.db"
        private const val DB_VERSION = 1
    }

    //Recebe as queries de criação do banco
    //Executa apenas uma vez, durente a crição do arquivo do banco "DB_NAME.db"
    //"db" é a instância aberta do arquivo para escrita de dados no banco
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBSchema.TaskTable.getCreateTableQuery())
    }

    //Execute sempre que o valor de DB_VERSION for alterado
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }
}