package com.example.tfc01.data

import android.content.Context
import com.example.tfc01.model.Task

object TaskDAOSingleton {
    private lateinit var taskList: ArrayList<Task> //Lista de tasks
    private lateinit var dbTask: DBTaskTableManager

    private fun initDAO(context: Context) {
        //Se não estiver inicializado
        if(!::taskList.isInitialized) {
            this.dbTask = DBTaskTableManager(context)
            this.taskList = dbTask.getAllTasks()
        }
    }

    //Retorna um ArrayList com todas as Tasks
    fun getAllTasks(context: Context): ArrayList<Task> {
        this.initDAO(context)
        return this.taskList
    }

    fun add(context: Context, task: Task): Int {
        this.initDAO(context)

        this.taskList.add(0, task)
        task.id = this.dbTask.insert(task)

        return 0
    }

    fun update(context: Context, task: Task): Int {
        this.initDAO(context)

        val pos = this.taskList.indexOf(task)
        val t = this.taskList[pos]
        t.setIsCompleted(task.getIsCompleted())
        this.dbTask.updateTaskCompleted(t)

        return pos
    }

    fun delete(context: Context, task: Task): Int {
        this.initDAO(context)

        val pos = this.taskList.indexOf(task)
        this.taskList.removeAt(pos)
        this.dbTask.delete(task)

        return pos
    }

}