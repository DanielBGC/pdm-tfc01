package com.example.tfc01.data

import android.content.ContentValues
import android.content.Context
import com.example.tfc01.model.Task

class DBTaskTableManager(context: Context) {
    companion object {
        const val WHEREID = "${DBSchema.TaskTable.ID} = ?"
        val COLUMNS =
            arrayOf(DBSchema.TaskTable.ID, DBSchema.TaskTable.DESCRIPTION, DBSchema.TaskTable.IS_URGENT, DBSchema.TaskTable.IS_COMPLETED)
        val ORDERBY = "${DBSchema.TaskTable.TIMESTAMP} DESC"
    }

    private val dbHelper = DBHelper(context)

    //INSERT INTO tasks (t_description, t_isUrgent) VALUES (?,?)
    fun insert(task: Task): Long {
        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.DESCRIPTION, task.getDescription())
        cv.put(DBSchema.TaskTable.IS_URGENT, if(task.getIsUrgent()){1}else{0})
        //cv.put(DBSchema.TaskTable.IS_COMPLETED, 0)

        //abre o banco
        val db = this.dbHelper.writableDatabase

        //Insere uma nova linha e retorna o id dessa linha
        val id = db.insert(DBSchema.TaskTable.TABLE_NAME, null, cv)
        db.close()
        return id
    }

    //DELETE FROM tasks WHERE t_id = ?
    fun delete(task: Task) {
        val db = this.dbHelper.writableDatabase

        db.delete(DBSchema.TaskTable.TABLE_NAME, WHEREID, arrayOf(task.id.toString()))
        db.close()
    }

    //UPDATE tasks SET t_isCompleted = ? WHERE t_id = ?
    fun updateTaskCompleted(task: Task) {
        val db = this.dbHelper.writableDatabase

        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.IS_COMPLETED, if(task.getIsCompleted()){1}else{0})

        db.update(DBSchema.TaskTable.TABLE_NAME, cv, WHEREID, arrayOf(task.id.toString()))
        db.close()
    }

    //SELECT t_id, t_description, t_isUrgent, t_isCompleted FROM tasks ORDER BY t_timestamp DESC
    fun getAllTasks(): ArrayList<Task> {
        val tasks: ArrayList<Task> = ArrayList()

        val db = this.dbHelper.writableDatabase

        val cursor = db.query(
            DBSchema.TaskTable.TABLE_NAME,
            COLUMNS,
            null,
            null,
            null,
            null,
            ORDERBY)

        while(cursor.moveToNext()) {
            val id = cursor.getLong(cursor.getColumnIndexOrThrow(DBSchema.TaskTable.ID))
            val description = cursor.getString(cursor.getColumnIndexOrThrow(DBSchema.TaskTable.DESCRIPTION))

            val isUrgentRaw = cursor.getString(cursor.getColumnIndexOrThrow(DBSchema.TaskTable.IS_URGENT))
            val isUrgent = isUrgentRaw == "1"

            val isCompletedRaw = cursor.getString(cursor.getColumnIndexOrThrow(DBSchema.TaskTable.IS_COMPLETED))
            val isCompleted = isCompletedRaw == "1"

            val task = Task(id, description, isUrgent, isCompleted)
            tasks.add(task)
        }

        db.close()
        return tasks
    }
}