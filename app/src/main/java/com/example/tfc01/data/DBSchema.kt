package com.example.tfc01.data

object DBSchema {
    object TaskTable {
        const val TABLE_NAME = "tasks"
        const val ID = "t_id"
        const val DESCRIPTION = "t_description"
        const val IS_URGENT = "t_isUrgent"
        const val IS_COMPLETED = "t_isCompleted"
        const val TIMESTAMP = "t_timestamp"

        fun getCreateTableQuery(): String {
            return """
                CREATE TABLE IF NOT EXISTS $TABLE_NAME (
                    $ID             INTEGER     PRIMARY KEY AUTOINCREMENT,
                    $DESCRIPTION    TEXT        NOT NULL,
                    $IS_URGENT      INTEGER     NOT NULL,
                    $IS_COMPLETED   INTEGER     DEFAULT 0,
                    $TIMESTAMP      TEXT        DEFAULT CURRENT_TIMESTAMP
                );
            """.trimIndent()
        }
    }
}