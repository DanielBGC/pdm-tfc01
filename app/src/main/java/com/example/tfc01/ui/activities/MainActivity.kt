package com.example.tfc01.ui.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.forEach
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc01.R
import com.example.tfc01.data.TaskDAOSingleton
import com.example.tfc01.model.Task
import com.example.tfc01.ui.dialogs.ConfirmDeleteTaskDialog
import com.example.tfc01.ui.dialogs.InputTaskUpdateDialog
import com.example.tfc01.ui.lists.TaskListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var lnvTaskList: LinearLayout     //LinearLayout para exibir as tasks

    private var etxtInput: EditText? = null            //Input
    private var switchBtn: SwitchCompat? = null        //Switch Button
    private var checkboxBtnFilter: CheckBox? = null    //Filtro (Exibe apenas as tarefas não concluídas)
    private lateinit var rvTaskList: RecyclerView
    private lateinit var taskListAdapter: TaskListAdapter
    private lateinit var fabDeleteTask: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.rvTaskList = findViewById(R.id.rvTaskList)
        this.etxtInput = findViewById(R.id.etxtInput)
        this.switchBtn = findViewById(R.id.switchBtn)
        this.checkboxBtnFilter = findViewById(R.id.checkBoxFilter)
        this.fabDeleteTask = findViewById(R.id.fabDeleteTask)

        this.rvTaskList.layoutManager = LinearLayoutManager(this)
        this.rvTaskList.setHasFixedSize(true)
        this.rvTaskList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        this.taskListAdapter = TaskListAdapter(TaskDAOSingleton.getAllTasks(this))
            .setOnChangeSelectionListener {
                if(it.size() > 0) {
                    this.fabDeleteTask.visibility = View.VISIBLE
                }
                else {
                    this.fabDeleteTask.visibility = View.GONE
                }
            }
            .setOnCompletedChangeListener {
                val pos = TaskDAOSingleton.update(baseContext, it)
                Log.d("DEBUGANDO", pos.toString())
            }

        this.rvTaskList.adapter = this.taskListAdapter

        //Listener para o checkbox de tarefas não feitas
        this.checkboxBtnFilter?.setOnCheckedChangeListener {
            _, isChecked -> this.taskListAdapter.filterCompletedTasks(!isChecked)
        }
    }

    //Função ao clicar no botão OK
    fun onClickOk(v: View) {
        //Valor do input
        val valueStr: String = this.etxtInput?.text.toString()

        if (valueStr.isNotEmpty()) {
            //Verifica se o Switch está ativo ou não (Urgente ou Não Urgente)
            val isUrgent: Boolean = switchBtn!!.isChecked

            //Instancia uma nova task da classe Task
            val task = Task(valueStr, isUrgent, false)

            //Adiciona a task criada na lista de tasks
            val pos = TaskDAOSingleton.add(baseContext, task)

            //Notifica ao adapter que houve uma inserção na lista
            this.rvTaskList.adapter?.notifyItemInserted(pos)

            //Faz um scroll até o topo da lista
            rvTaskList.scrollToPosition(pos)

            //Limpa o valor do input
            this.etxtInput?.setText("")

        } else {
            //Mostra uma mensagem para o usuário, se o input estiver vazio
            Toast.makeText(this, R.string.input_empty, Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickDeleteSelectedTasks(v: View) {
        object : ConfirmDeleteTaskDialog(this) {
            override fun onConfirm() {
                taskListAdapter.selectedTasks.forEach { _, value ->
                    val pos = TaskDAOSingleton.delete(baseContext, value)
                    taskListAdapter.notifyItemRemoved(pos)
                }
                taskListAdapter.selectedTasks.clear()
                fabDeleteTask.visibility = View.GONE
            }
        }
    }
}