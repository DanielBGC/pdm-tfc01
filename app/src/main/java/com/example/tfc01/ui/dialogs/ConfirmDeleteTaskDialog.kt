package com.example.tfc01.ui.dialogs

import android.app.AlertDialog
import android.content.Context
import android.widget.EditText
import com.example.tfc01.R

abstract class ConfirmDeleteTaskDialog(private val context: Context) {
    init {
        AlertDialog.Builder(this.context)
            .setTitle(R.string.delete_task_operation)
            .setMessage(R.string.confirm_delete_task)
            .setPositiveButton(R.string.yes) { _, _ ->
                this.onConfirm()
            }
            .setNegativeButton(R.string.no, null)
            .create().show()

    }

    abstract fun onConfirm()
}