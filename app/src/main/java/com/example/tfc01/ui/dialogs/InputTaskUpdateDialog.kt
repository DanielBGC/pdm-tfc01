package com.example.tfc01.ui.dialogs

import android.app.AlertDialog
import android.content.Context
import android.widget.EditText
import androidx.appcompat.widget.SwitchCompat
import com.example.tfc01.R

abstract class InputTaskUpdateDialog(private val context: Context) {
    //private val etxtNewDescription = EditText(this.context)
    private val switchBtnCompleted = SwitchCompat(this.context)
    private var isChecked: Boolean? = null

    init {
        this.switchBtnCompleted.setOnCheckedChangeListener {
                _, isChecked -> this.isChecked = isChecked
        }

        AlertDialog.Builder(this.context)
            .setTitle(R.string.update_task_operation)
            .setMessage("Is the task completed?")
            .setView(switchBtnCompleted)
            .setPositiveButton(R.string.ok) { _, _ ->
                this.onClickOk(this.isChecked!!)
            }
            .create().show()
    }

    abstract fun onClickOk(isCompleted: Boolean)
}