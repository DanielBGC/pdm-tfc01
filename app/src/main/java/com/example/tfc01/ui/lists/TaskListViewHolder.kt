package com.example.tfc01.ui.lists

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc01.R
import com.example.tfc01.model.Task

// O construtor da ViewHolder deve sempre definir seu itemView para a superclasse
class TaskListViewHolder(
    itemView: View,
    private val adapter: TaskListAdapter
    ) :  RecyclerView.ViewHolder(itemView),
         View.OnClickListener,
         View.OnLongClickListener {

    private val txtTaskDescription: TextView = itemView.findViewById(R.id.txtTaskDescription)
    private val checkboxBtn: CheckBox = itemView.findViewById(R.id.checkboxBtn)
    private lateinit var task: Task

    init {
        this.checkboxBtn.setOnCheckedChangeListener { _, isChecked ->
            this.task.setIsCompleted(isChecked)
            this.adapter.getOnCompletedChangeListener()?.onCompleteTask(this.task)
        }

        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    // bind -> será chamado toda vez que houver necessidade de reciclar os dados de um itemView
    fun bind(task: Task) {
        this.task = task
        this.txtTaskDescription.text = this.task.getDescription()
        this.checkboxBtn.isChecked = this.task.getIsCompleted()
        itemView.isSelected = this.adapter.selectedTasks.get(this.task.id.toInt()) != null
    }

    override fun onClick(v: View?) {
        this.adapter.getOnClickTaskListener()?.onRequestTask(this.task)
        this.adapter.lastRequestedTask = this.task
    }

    override fun onLongClick(v: View?): Boolean {
        itemView.isSelected = !itemView.isSelected
        if(this.adapter.selectedTasks.get(this.task.id.toInt()) != null)
            this.adapter.selectedTasks.delete(this.task.id.toInt())
        else
            this.adapter.selectedTasks.append(this.task.id.toInt(), this.task)

        val newSelectedArray = this.adapter.selectedTasks.clone()
        this.adapter.getOnChangeSelectionListener()?.onChangeSelection(newSelectedArray)
        return true
    }
}