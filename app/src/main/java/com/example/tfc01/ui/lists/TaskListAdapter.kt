package com.example.tfc01.ui.lists

import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tfc01.R
import com.example.tfc01.model.Task

class TaskListAdapter(
    private var taskList: ArrayList<Task>
) : RecyclerView.Adapter<TaskListViewHolder>() {

    companion object {
        const val URGENT = 1
        const val NOT_URGENT = 0
    }

    private var tempTaskList: ArrayList<Task> = this.taskList

    private var listener: OnClickTaskListener? = null
    private var selectionListener: OnChangeSelectionListener? = null
    private var completeListener: OnCompletedChangeListener? = null
    val selectedTasks: SparseArray<Task> = SparseArray()

    fun interface OnClickTaskListener {
        fun onRequestTask(task: Task)
    }

    fun interface OnChangeSelectionListener {
        fun onChangeSelection(selectedIds: SparseArray<Task>)
    }

    fun interface OnCompletedChangeListener {
        fun onCompleteTask(task: Task)
    }

    var lastRequestedTask: Task? = null

    // onCreateViewHolder -> seu objetivo é, quando necessário, criar novos itemViews.
    // Retorna ViewHolder que contém o objeto de itemView instanciado. Recebe dois parâmetros:
    // "parent" -> o ViewGroup do RecyclerView onde o itemView será posicionado
    // "viewType" -> uma flag (int) que representa o tipo de view que deve ser instanciada,
    // quando a lista compreende views de formatos diferentes. Parâmetro alimentado pelo método
    // getItemViewType(pos).
    // Implementações básicas levam apenas 3 passos para implementar onCreateViewHolder:
    // 1) Resgatar o LayoutInflater a partir de parent
    // 2) Inflar o itemView usando o LayoutInflater
    // 3) Instanciar a ViewHolder a partir do ItemView e retornar a ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val res = if(viewType == URGENT) {
            R.layout.view_task_urgent
        } else {
            R.layout.view_task_not_urgent
        }

        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(res, parent, false)

        return TaskListViewHolder(itemView, this)
    }

    // onBindViewHolder -> Sempre quando há a necessidade de atribuir/reatribuir (reciclar) valores
    // no itemView este método será executado. Responsável por reciclar itemViews.
    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        holder.bind(this.taskList[position])
    }

    // getItemCount -> ditará a quantidade de views a serem mostradas no RecyclerView.
    // O num. retornado deve ser >= 0 ou <= tamanho da base (channelList)
    override fun getItemCount(): Int {
        return this.taskList.size
    }

    // AULA 24-11-2021
    override fun getItemViewType(position: Int): Int {
        return if(this.taskList[position].getIsUrgent()) {
            URGENT
        } else {
            NOT_URGENT
        }
    }

    //Filtra todas as tarefas feitas
    fun filterCompletedTasks(showAll: Boolean) {
        if(showAll) {
            this.taskList = this.tempTaskList
        } else {
            this.taskList = this.taskList.filter {
                !it.getIsCompleted()
            } as ArrayList<Task>
        }
        this.notifyDataSetChanged()
    }

    //------------------------------
    fun setOnClickTaskListener(listener: OnClickTaskListener?): TaskListAdapter {
        this.listener = listener
        return this
    }
    fun setOnCompletedChangeListener(completeListener: OnCompletedChangeListener): TaskListAdapter {
        this.completeListener = completeListener
        return this
    }

    fun getOnCompletedChangeListener(): OnCompletedChangeListener? {
        return this.completeListener
    }
    fun getOnClickTaskListener(): OnClickTaskListener? {
        return this.listener
    }

    fun setOnChangeSelectionListener(selectionListener: OnChangeSelectionListener?): TaskListAdapter {
        this.selectionListener = selectionListener
        return this
    }
    fun getOnChangeSelectionListener(): OnChangeSelectionListener? {
        return this.selectionListener
    }


}